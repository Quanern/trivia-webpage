import VueRouter from 'vue-router'
import Start from "./components/Start/Start";
import Question from "./components/Question/Question";
import Result from "./components/Result/Result";

const routes = [
    {
        path: '/',
        name: 'home',
        component: Start,
    },
    {
        path: '/play',
        name: 'play',
        component: Question,
        props: true
    },
    {
        path: '/score',
        name: 'score',
        component: Result,
        props: true
    }
]

const router = new VueRouter({ routes })

export default router
