/**
 * Function that fetches the JSON-response from the open api that creates the quiz
 * @param numberOfGames number of games the user wants for the trivia
 * @param difficulty the difficulty of the question for the trivia
 * @param categoryId the category the user wants for the trivia
 * @returns {Promise<any | void>} the json-response.
 */
export function fetchQuestions(numberOfGames, difficulty, categoryId) {
    return fetch(`https://opentdb.com/api.php?amount=${numberOfGames}&category=${categoryId}&difficulty=${difficulty}`)
        .then(response => {
            return response.json()
        })
        .catch(error => {
            console.log(error.message)
        })
}
