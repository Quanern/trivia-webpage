# Trivia Quiz
#### By Quan Tran and Renze Stolte
This project is a website written with Vue as a trivia game. The project gets it's questions and answers from the API
https://opentdb.com/api_config.php. The project consists of 3 views, the start page, the game, and the result screen. 
There is also a navbar available on every view.

This project is hosted on heroku and can be found here: https://trivia-quiz-quan-renze.herokuapp.com/

## Start page
The start page is the landing page of the project. Here the user can choose how many questions they want for the quiz,
the difficulty and what kind of questions they want. The user then clicks on the "Start the game"-button, and the game
will begin.

## Game page
This page is the game. It consists of 3 or 5 boxes, depending on the type of question. There are two kinds of questions,
multiple choice and true or false. The page will present the question and the possible answers for the question. Only 
one answer is correct for each question.

## Result page
The result page shows how many points the user got, and all the questions and choices for each question. If the user
chose correct, the correct choice, and the user's choice, will have a green background. If the user chose wrong, then
their choice will have a red background, and the correct choice will have a green background. The user gets 10 points
for every correct answer they get.
